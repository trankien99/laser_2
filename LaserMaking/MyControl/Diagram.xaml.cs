﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LaserMaking.Model;
namespace LaserMaking.MyControl
{
    /// <summary>
    /// Interaction logic for Diagram.xaml
    /// </summary>
    public partial class Diagram : UserControl
    {
        public int Cols { get; set; }
        public int Rows { get; set; }
        public Diagram()
        {
            InitializeComponent();
        }
        public void Render(int Cols, List<Model.Entity> entities)
        {
            this.Cols = Cols;
            this.Rows = entities.Count;
            grMain.Children.Clear();
            grMain.ColumnDefinitions.Clear();
            grMain.RowDefinitions.Clear();
            for (int i = 0; i < Cols; i++)
            {
                grMain.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < Rows; i++)
            {
                grMain.RowDefinitions.Add(new RowDefinition());
            }
            for (int x = 0; x < grMain.ColumnDefinitions.Count; x++)
            {
                for (int y = 0; y < grMain.RowDefinitions.Count; y++)
                {
                    StackPanel stackPanel = new StackPanel();
                    stackPanel.Orientation = Orientation.Vertical;

                    StackPanel subStackPanel = new StackPanel();
                    subStackPanel.Orientation = Orientation.Horizontal;

                    Label lb1 = new Label();
                    lb1.Width = 40;
                    lb1.FontSize = 12;
                    lb1.Content = Convert.ToString(y + 1);

                    Label lb2 = new Label();
                    lb2.Width = 80;
                    lb2.FontSize = 12;
                    lb2.Content = entities[y].Name;

                    Label lb3 = new Label();
                    lb3.Width = 80;
                    lb3.FontSize = 12;
                    lb3.Content = entities[y].Start.ToString();

                    Label lb4 = new Label();
                    lb4.Width = 80;
                    lb4.FontSize = 12;
                    lb4.Content = entities[y].End.ToString();

                    

                    subStackPanel.Children.Add(lb1);
                    subStackPanel.Children.Add(lb2);
                    subStackPanel.Children.Add(lb3);
                    subStackPanel.Children.Add(lb4);


                    stackPanel.Children.Add(subStackPanel);

                    Grid grid = new Grid();
                    Grid.SetColumn(grid, x);
                    Grid.SetRow(grid, y);
                    //grid.Margin = new Thickness(2);
                    grid.Background = System.Windows.Media.Brushes.LightGray;
                    grid.MouseDown += PClick;
                    grid.Children.Add(stackPanel);
                    grMain.Children.Add(grid);

                }
            }
        }

        public void SetColor(int Cols, int Rows, System.Windows.Media.Brush color)
        {
            foreach (var item in grMain.Children)
            {
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if(Cols == col && row == Rows)
                {
                    (item as Grid).Background = color;
                }
            }
        }
        public void SetFontSize(int fontSize)
        {
            foreach (var item in grMain.Children)
            {
                Label label = (item as Grid).Children[0] as Label;
                label.FontSize = fontSize;
            }
        }
        public void SetLabel(int Cols, int Rows, string content)
        {
            
            foreach (var item in grMain.Children)
            {
                int i = grMain.Children.Count;
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if (Cols == col && row == Rows)
                {
                    StackPanel st = (item as Grid).Children[0] as StackPanel;
                    TextBlock text = (st as StackPanel).Children[1] as TextBlock;
                    text.Text = content;
                }
            }
        }
        public string GetLabel(int Cols, int Rows)
        {
            string content = string.Empty;
            foreach (var item in grMain.Children)
            {
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if (Cols == col && row == Rows)
                {
                    Label label = (item as Grid).Children[0] as Label;
                    content = label.Content as string;
                }
            }
            return content;
        }
        public event MouseButtonEventHandler PClick;
        protected void PClick_Click(object sender, MouseButtonEventArgs e)
        {
            //bubble the event up to the parent
            if (this.PClick != null)
                this.PClick_Click(this, e);
        }

        internal void SetColor(int i, int v, System.Drawing.Brush green)
        {
            throw new NotImplementedException();
        }

        internal void SetColor(int i, int v, object green)
        {
            throw new NotImplementedException();
        }
    }
}
