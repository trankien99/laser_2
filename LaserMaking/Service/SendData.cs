﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserMaking.Service
{
    public class SendData
    {
        public static SerialPort _Serial = new SerialPort();
        public string _Format_1 = "125H                END\r\n";
        public string _Format_SN = new string(new char[25]).Replace('\0', ' ');
        public string _Format_MAC = new string(new char[12]).Replace('\0', ' ');
        public string _Format_Station = new string(new char[12]).Replace('\0', ' ');
        public string _Format_Error_Code = new string(new char[6]).Replace('\0', ' ');
        private Properties.Settings _Param = Properties.Settings.Default;
        public int Connect(string COM)
        {
            if (_Serial.IsOpen)
            {
                _Serial.Close();
            }
            _Serial.PortName = COM;
            _Serial.ReadTimeout = 2000;
            _Serial.WriteTimeout = 2000;
            _Serial.BaudRate = 9600;
            try
            {
                _Serial.Open();
            }
            catch (Exception er)
            {
                Console.WriteLine(er.Message);
                return -1;
            }
            return 0;
        }
        public int Disconnect()
        {
            if (_Serial.IsOpen)
            {
                _Serial.Close();
            }
            return 0;
        }
        public string ConvertData(int id_format, string sn = "", string mac = "", string status = "PASS", string error_code = "")
        {
            string content = string.Empty;
            string sn_send = "";
            string mac_send = "";
            string station_send = "";
            string error_code_send = "";
            if (sn.Length < _Format_SN.Length)
            {
                string space_sn = _Format_SN.Substring(sn.Length, _Format_SN.Length - sn.Length);
                sn_send = sn + space_sn;
            }
            if (mac.Length < _Format_MAC.Length)
            {
                string space_mac = _Format_MAC.Substring(mac.Length, _Format_MAC.Length - mac.Length);
                mac_send = mac + space_mac;
            }
            else
                mac_send = mac;
            if (_Param.STATION_NAME.Length < _Format_Station.Length)
            {
                string space_station = _Format_Station.Substring(_Param.STATION_NAME.Length, _Format_Station.Length - _Param.STATION_NAME.Length);
                station_send = space_station + _Param.STATION_NAME;
            }
            else
                station_send = _Param.STATION_NAME;
            if (error_code.Length < _Format_Error_Code.Length)
            {
                string space_error_code = _Format_Error_Code.Substring(error_code.Length, _Format_Error_Code.Length - error_code.Length);
                error_code_send = space_error_code + error_code;
            }
            else
                error_code_send = error_code;
            switch (id_format)
            {
                case 1:
                    content = _Format_1;
                    break;
                case 2:
                    content = sn_send + mac_send + "END\r\n";
                    break;
                case 3:
                    if (status == "PASS")
                        content = sn_send + station_send+"\r\n";
                    else
                        content = sn_send + station_send + error_code_send+"\r\n";
                    break;
            }
            return content;
        }
        public StarttestStatus sendData(string content,string res = "PASS")
        {
            StarttestStatus status = StarttestStatus.FAIL;
            if (!_Param.TEST)
            {
                if (_Serial.IsOpen)
                {
                    _Serial.DiscardInBuffer();
                    _Serial.DiscardOutBuffer();
                    _Serial.Write(content);
                    //byte[] buff = new byte[27];

                    //_Serial.Read(buff, 0, buff.Length);
                    //string recived = ASCIIEncoding.ASCII.GetString(buff);
                    string recived = receive();
                    if (res == "PASS")
                    {
                        if (recived.ToUpper().Contains("PASS"))
                        {
                            status = StarttestStatus.OK;
                        }
                    }
                    else
                    {
                        if (recived.ToUpper().Contains("ERRO"))
                        {
                            status = StarttestStatus.OK;
                        }
                    }
                  
                }
                return status;
            }
            else
                return StarttestStatus.TEST;
        }
        public string receive()
        {
            string content = string.Empty;
            //if (_Serial.IsOpen)
            //{
                try
                {
                    while (true)
                    {
                        int c = _Serial.ReadChar();
                        content += Convert.ToChar(c);
                    }
                }
                catch { }
            //}
            return content;
        }
    }
    public enum StarttestStatus
    {
        OK, 
        FAIL,
        TEST
    }
}
