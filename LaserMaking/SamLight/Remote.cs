﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAMLIGHT_CLIENT_CTRL_OCXLib;
using System.Windows;

namespace LaserMaking.SamLight
{
    public class Remote
    { 

        AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl m_samlight;
        public Remote(AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl p_samlight)
        {
            m_samlight = p_samlight;
        }
        public int CheckSamLight()
        {
            if (m_samlight.ScIsRunning() == 0)
            {
                MessageBox.Show("SAMLight not found", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return -1;
            }
            else
                return 1;
        }
        public int ChangeTextEntity(string EntityName, string text)
        {
            if (m_samlight.ScIsRunning() == 0)
            {
                MessageBox.Show("SAMLight not found", "Warning");
                return -1;
            }

            return m_samlight.ScChangeTextByName(EntityName, text);
        }
        public int MoveXYZ(string EntityName, double X, double Y, double Z)
        {
            int moved = m_samlight.ScTranslateEntity(EntityName, X, Y, Z);
            return moved;
        }
        public int turnOnEngraving(int Do)
        {
            int s = m_samlight.ScSwitchLaser(Do);
            return s;
        }
        public void stop()
        {
            m_samlight.ScStopMarking();
        }
        public void stop2()
        {
            m_samlight.ScExecCommand((int)ScComSAMLightClientCtrlExecCommandConstants.scComSAMLightClientCtrlExecCommandMotionStopMove);
        }
        public int setPen(int num_pen)
        {
            int start = m_samlight.ScSetPen(num_pen);
            return start;
        }
        public int waiteEngrave(string entity,int Do)
        {

                int m = m_samlight.ScMarkEntityByName(entity, Do);
                return m;

        }
        public int showMark()
        {
            int mark = m_samlight.ScShowApp(0);
            return mark;
        }
        public void setPower(int pen, double power)
        {
            m_samlight.ScSetPen(pen);
            m_samlight.ScSetDoubleValue((int)ScComSAMLightClientCtrlValueTypes.scComSAMLightClientCtrlDoubleValueTypeLaserPower, power);
        }
        public int loadFileDesign(string path,string model_name)
        {
            if (m_samlight.ScIsRunning() == 0)
            {
                MessageBox.Show("SAMLight not found", "Warning", MessageBoxButton.OK);
                return -1;
            }
            string file_name = System.IO.Path.GetFileName(path).Split('.')[0];
            if(file_name!= model_name)
            {
                MessageBox.Show("file design khong dung\n file design is incorrect");
                return -1;
            }
            int load_entities = 1;
            int overwrite_entities = 1;
            int load_materials = 1;

            //if (CHECK_ENTITIES.Checked == true)
            //    load_entities = 1;
            //if (CHECK_OVERWRITE_ENTITIES.Checked == true)
            //    overwrite_entities = 1;
            //if (CHECK_MATERIALS.Checked == true)
            //    load_materials = 1;

            m_samlight.ScLoadJob(path, load_entities, overwrite_entities, load_materials);
            return 1;
        }
    }
}
