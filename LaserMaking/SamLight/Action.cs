﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace LaserMaking.SamLight
{
    public class Action
    {
        private Remote _Laser = null;
        private AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl axScSamlightClientCtrl1;
        private Views.MainWindow _MainWindow = null;
        private Service.SendData _IT = new Service.SendData();
        public Action( Views.MainWindow mainWindow)
        {
            _MainWindow = mainWindow;
            axScSamlightClientCtrl1 = new AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl();
            _Laser = new Remote(axScSamlightClientCtrl1);
        }
        public int Engraving(string snRead)
        {
            _MainWindow.ShowInfor("Making ...", Brushes.Yellow, "");
            string sn = snRead.Split('-')[0];
            int finish = _Laser.waiteEngrave("", 1);
            if (finish==1)
            {
                string data = _IT.ConvertData(3, sn: sn, status: "PASS");
                _IT.sendData(data, res: "PASS");
            }
            else
            {
                string data = _IT.ConvertData(3, sn: sn, status: "FAIL", error_code: "MkF");
                _IT.sendData(data, res: "FAIL");
            }
              
            return finish;
        }
        public int setEntities(Model.Model model, string snRead)
        {
            if (snRead != null)
            {
                if (snRead.Length == model.LengthSN)
                {
                    string sn_full = snRead.Split('-')[0];
                    string data = _IT.ConvertData(2, sn_full, sn_full);
                    Service.StarttestStatus res = _IT.sendData(data);
                    if (res == Service.StarttestStatus.OK || res == Service.StarttestStatus.TEST)
                    {
                        foreach (Model.Entity entity in model.Entities)
                        {
                            string nameEntity = entity.Name;
                            string value = snRead.Substring(entity.Start, entity.End - entity.Start);
                            int resSetEntity = _Laser.ChangeTextEntity(nameEntity, value);
                            if (resSetEntity != 1)
                            {
                                string notification = $"Entity {nameEntity} not found!\n(Thuộc tính {nameEntity} không được tìm thấy. Xem lại file design và set up hàng !)";
                                
                                _MainWindow.ShowInfor("FAIL", Brushes.Red, notification);
                                return -1;
                            }

                        }
                        string wyv = getWYV();
                        if (wyv == null)
                            return -1;
                        else
                            return 1;
                    }
                    else
                    {
                        string notification = "IT kiem tra loi. Xem terminal de biet chi tiet loi";
                        _MainWindow.ShowInfor("FAIL", Brushes.Red, notification);

                        return -1;
                    }
                }
                else
                {
                    string notification = "FORMAT SN IS INCORRECT !\n(Sn đọc được không đúng ! Hãy xảo lại !)";
                    _MainWindow.ShowInfor("FAIL", Brushes.Red, notification);
                    return -1;
                }
            }
            else
            {
                string notification = "FORMAT SN IS INCORRECT !\n(Sn đọc được không đúng ! Hãy xảo lại !)";
                _MainWindow.ShowInfor("FAIL", Brushes.Red, notification);
                return -1;
            }
        }
        private string getWYV()
        {
            DateTime now = DateTime.Now;
            var currentCulture = CultureInfo.CurrentCulture;
            string str_week = "";
            var weekNo = currentCulture.Calendar.GetWeekOfYear(
                            now,
                            currentCulture.DateTimeFormat.CalendarWeekRule,
                            currentCulture.DateTimeFormat.FirstDayOfWeek);
            if (weekNo < 10)
                str_week = "0" + weekNo.ToString();
            else
                str_week = weekNo.ToString();
            var yearNo = currentCulture.Calendar.GetYear(now);
            string wyv = $"{yearNo.ToString().Substring(2, 2)}{str_week}V";
            if (wyv.Length != 5)
                return null;
            else
                return wyv;
        }
    }
}
