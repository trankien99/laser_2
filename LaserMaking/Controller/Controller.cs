﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace LaserMaking.Controller
{
    public class Controller
    {
        private Views.MainWindow _Main = null;
        AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl axScSamlightClientCtrl1;
        SamLight.Remote _Laser = null; 
        public Controller(Views.MainWindow mainWindow)
        {
            _Main = mainWindow;
            axScSamlightClientCtrl1 = new AxSAMLIGHT_CLIENT_CTRL_OCXLib.AxScSamlightClientCtrl();
            //_Main.wfhSample.Child.Controls.Add(axScSamlightClientCtrl1);
            _Laser = new SamLight.Remote(axScSamlightClientCtrl1);
        }
        public void ShowQty()
        {
            var res = Database.db.CountQty();
            _Main.lb_qty.Content = res.Count().ToString();
        }
        public int Start()
        {
            string modelName = _Main.cbModelsName.Text;
            if (modelName == "")
            {
                MessageBox.Show("Please choose model !\n(Hãy chọn model !)", "Infomation", MessageBoxButton.OK, MessageBoxImage.Information);
                return -1;
            }
            //int resSamLight = _Laser.CheckSamLight();
            //if (resSamLight != -1)
            //{
            //    _Main.bd_statusSamLight.Background = Brushes.Red;
            //    _Main.lb_statusSamLight.Content = "ERROR";
            //    return -1;
            //}
                
            _Main.model = Model.Model.LoadModel(modelName);
            if (_Main.model == null)
            {
                MessageBox.Show("cant load model !\n(Không thể load model !)", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            _Main.btRun.Content = _Main.btRun.Content == _Main.FindResource("Play") ? _Main.FindResource("Stop") : _Main.FindResource("Play");
            _Main.cbModelsName.IsEnabled = false;
            _Main.IsRuning = true;
            _Main.btRun.IsEnabled = true;
            _Main.st_scan.IsEnabled = true;
            return 1;
        }
        public void stop()
        {
            _Main.btRun.Content = _Main.btRun.Content == _Main.FindResource("Play") ? _Main.FindResource("Stop") : _Main.FindResource("Play");
            _Main.cbModelsName.IsEnabled = true;
            _Main.IsRuning = false;
            _Main.btRun.IsEnabled = false;
            _Main.st_scan.IsEnabled = false;
            _Main.btRun.IsEnabled = true;
            _Main.IsEnabled = true;
        }
    }
}
