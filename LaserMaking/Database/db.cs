﻿using System.Data.SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace LaserMaking.Database
{
    public class db
    {
        private static SQLiteConnection _con = new SQLiteConnection();
        public static void createConection()
        {

            string _strConnect = $"Data Source={DefineTbl.DBName}.splite;Version=3;";
            _con.ConnectionString = _strConnect;
            _con.Open();

           
        }
        public static bool CreateTable(string TableName, string[] TableContent)
        {
            if (!File.Exists($"{DefineTbl.DBName}.sqlite"))
            {
                SQLiteConnection.CreateFile($"{DefineTbl.DBName}.sqlite");
                bool rep = true;
                createConection();
                string contentTbl = string.Empty;
                for (int i = 0; i < TableContent.Length; i++)
                {
                    contentTbl += TableContent[i];
                    if (i != TableContent.Length - 1)
                    {
                        contentTbl += ",";
                    }
                }
                using (SQLiteCommand command = _con.CreateCommand())
                {
                    try
                    {
                        command.CommandText = string.Format("CREATE TABLE IF NOT EXISTS {0} ({1});", TableName, contentTbl);
                        var rsl = command.ExecuteNonQuery();
                    }
                    catch (SQLiteException ex)
                    {
                        rep = false;
                    }
                }
                _con.Close();
                return rep;
            }
            else
                return true;
            
        }
        public static bool Insert(string TableName, string[] Values)
        {
            bool rep = true;
            createConection();
            string values = string.Empty;
            for (int i = 0; i < Values.Length; i++)
            {
                values += string.Format("{0}", Values[i]);
                if (Values.Length - 1 != i)
                {
                    values += ",";
                }
            }
            using (SQLiteCommand command = _con.CreateCommand())
            {
                try
                {
                    command.CommandText = string.Format("INSERT INTO {0} VALUES({1})", TableName, values);
                    command.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    rep = false;
                }
            }
            _con.Close();
            return rep;
        }
        public static List<object[]> FindMany(string TableName, string ColSearch, string ColCondition = null, string Values = null)
        {
            createConection();
            List<object[]> result = new List<object[]>();
            using (SQLiteCommand command = _con.CreateCommand())
            {
                try
                {
                    if (ColCondition == null)
                    {
                        command.CommandText = string.Format("SELECT {0} FROM {1}", ColSearch, TableName);
                    }
                    else
                    {
                        command.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2}={3}", ColSearch, TableName, ColCondition, Values);
                    }
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        object[] obj = new object[reader.VisibleFieldCount];
                        for (int i = 0; i < obj.Length; i++)
                        {
                            obj[i] = reader.GetValue(i);
                        }
                        result.Add(obj);
                    }
                    reader.Close();
                }
                catch (SQLiteException ex)
                {
                }

            }
            _con.Close();
            return result;
        }
        public static List<object[]> CountQty()
        {
            //DataSet ds = new DataSet();
            //if (File.Exists($"{DefineTbl.DBName}.sqlite"))
            //{
            //    createConection();
            //    SQLiteDataAdapter da = new SQLiteDataAdapter($"select count({Database.DefineTbl.ModelName}) from {Database.DefineTbl.TblName} where {Database.DefineTbl.ColStatus}='PASS'", _con);

            //    da.Fill(ds);
            //    _con.Close();
            //}

            //return ds;
            var data = FindMany(DefineTbl.TblName, DefineTbl.ColSN);
            return data;

        }
    }
    struct DefineTbl
    {
        public static string TblName = "Laser";
        public static string ColSN = "SN";
        public static string ColStatus = "Status";
        public static string ColErrorCode = "Error_code";
        public static string DBName = "db";
        public static string ModelName = "Model_name";
    }
}
