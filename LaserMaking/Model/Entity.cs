﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserMaking.Model
{
    public class Entity
    {
        public int LenSn { get; set; }
        public string Name { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
        public Entity Copy()
        {
            Entity entity = new Entity();
            entity.Name = this.Name;
            entity.Start = this.Start;
            entity.End = this.End;
            entity.LenSn = this.LenSn;
            return entity;
        }
    }
    
}
