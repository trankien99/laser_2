﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaserMaking.Model;
using Newtonsoft.Json;
namespace LaserMaking.Model
{
    public class Model
    {
        private static Properties.Settings _Param = Properties.Settings.Default;
        public DateTime Modified { get; set; }
        public string Name { get; set; }
        public string DesignFile { get; set; }
        public int LengthSN { get; set; }
        public List<Entity> Entities { get; set; }
        public Model()
        {
            Entities = new List<Entity>();
        }
        public int Save()
        {
            this.Modified = DateTime.Now;
            string savePath = $"{_Param.SAVE_PATH}/{this.Name}";
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            string configPath = $"{savePath}/{this.Name}.json";
            string js = JsonConvert.SerializeObject(this);
            File.WriteAllText(configPath, js);
            return 1;
        }
        public static Model LoadModel(string ModelName)
        {
            string path = $"{_Param.SAVE_PATH}/{ModelName}/{ModelName}.json";
            if (File.Exists(path))
            {
                string s = File.ReadAllText(path);
                Model model = null;
                try
                {
                    model = JsonConvert.DeserializeObject<Model>(s);
                }
                catch (Exception ex)
                {
                    return null;
                }
                if (model != null)
                {
                    return model;
                }
                else
                    return null;
            }
            else
                return null;
        }
    }
    
}
