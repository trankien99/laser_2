﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LaserMaking.Views
{
    /// <summary>
    /// Interaction logic for NewModel.xaml
    /// </summary>
    public partial class NewModel : Window
    {
        private Model.Model _Model = new Model.Model();
        private SetUpModel _SetUpModel = null;
        public NewModel( SetUpModel setUpModel)
        {
            InitializeComponent();
            _SetUpModel = setUpModel;
        }

        private void bt_openFileDesign_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            Nullable<bool> result = openFileDialog.ShowDialog();
            if (result == true)
            {
                try
                {
                    string filePath = openFileDialog.FileName;
                    string file_name = System.IO.Path.GetFullPath(filePath);
                    txt_fileDesign.Text = file_name;
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void bt_save_Click(object sender, RoutedEventArgs e)
        {
            int res = _Model.Save();
            if (res == 1)
            {
                _SetUpModel.cbModelsName.Items.Add(_Model.Name);
                MessageBox.Show("Save successfully !", "INFOR", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void txt_modelName_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            _Model.Name = tb.Text;
        }

        private void txt_fileDesign_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            _Model.DesignFile = tb.Text;
        }
    }
}
