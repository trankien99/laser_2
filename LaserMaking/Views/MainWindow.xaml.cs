﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LaserMaking.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SamLight.Action _Action = null;
        public Model.Model model = null;
        private Controller.Controller _Controller = null;
        public bool IsRuning = false;
        private Properties.Settings _Param = Properties.Settings.Default;
        private System.Timers.Timer _Timer = new System.Timers.Timer(50);
        public MainWindow()
        {
            InitializeComponent();
            _Action = new SamLight.Action(this);
            _Controller = new Controller.Controller(this);
            AddItemsCb();
            bool rep = Database.db.CreateTable(Database.DefineTbl.TblName, new string[] { Database.DefineTbl.ModelName, Database.DefineTbl.ColSN, Database.DefineTbl.ColStatus, Database.DefineTbl.ColErrorCode });
            //_Timer.Elapsed += OnTimedEvent;
            //_Timer.Enabled = true;
        }
        //private void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    _Timer.Enabled = false;
        //    _Controller.ShowQty();
        //    _Timer.Enabled = true;
        //}

        private void AddItemsCb()
        {
            var models = Directory.GetDirectories(_Param.SAVE_PATH);
            for (int i = 0; i < models.Count(); i++)
            {
                string model = System.IO.Path.GetFileName(models[i]);
                cbModelsName.Items.Add(model);
            }
        }
        private void btMachineIssue_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btAlarm_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btModelManager_Click(object sender, RoutedEventArgs e)
        {
            SetUpModel setUpModel = new SetUpModel();
            setUpModel.ShowDialog();
        }
        //private BitmapImage readImageSource(string pathImg)
        //{
        //    BitmapImage bimage = new BitmapImage();
        //    bimage.BeginInit();
        //    bimage.UriSource = new Uri(pathImg, UriKind.Relative);
        //    bimage.EndInit();
        //    return bimage;
        //}
        private void btStart(object sender, RoutedEventArgs e)
        {
            bool a = IsRuning;
            if (!IsRuning)
            {
                int res = _Controller.Start();
                if (res == -1)
                    return;

            }
            else
            {
                _Controller.stop();

            }
            

        }

        private void txt_sn_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = new TextBox();
            if (tb.Text != null)
            {
                bt_making.IsEnabled = true;
                ShowInfor("Making", Brushes.Green, "Click here or press 'ENTER' in keyboard to making");
            }
            else
                bt_making.IsEnabled = false;

        }

        private void txt_sn_KeyDown(object sender, KeyEventArgs e)
        {

            string sn = txt_sn.Text.Split('-')[0];
            if (e.Key == Key.Enter)
            {
                Database.db.Insert(Database.DefineTbl.TblName, new string[] { "\'" + cbModelsName.Text + "\'", "\'" + sn + "\'", "\'" + "PASS" + "\'", "\'" + "" + "\'" });
                _Controller.ShowQty();
                int resSetEntity = _Action.setEntities(model, txt_sn.Text);
                if (resSetEntity == 1)
                {
                    int resMaking = _Action.Engraving(txt_sn.Text);
                    if (resMaking == 1)
                    {
                        ShowInfor("Finish", Brushes.Green, " Please scan barcode to continue making !");
                    }
                }
                //else
                //{
                //    ShowInfor("fail", Brushes.Red, "fdgfdghgj");
                //}
                
            }
        }
        public void ShowInfor(string status,Brush color, string infor)
        {
            
            bt_making.Content = status;
            bt_making.Background = color;
            txt_infor.Content = infor;
            
        }
    }
}
