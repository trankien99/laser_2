﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LaserMaking.Views
{
    /// <summary>
    /// Interaction logic for SetUpModel.xaml
    /// </summary>
    public partial class SetUpModel : Window
    {
        private int _Stt = 0;
        private Model.Entity _Entity = new Model.Entity();
        private Properties.Settings _Param = Properties.Settings.Default;
        private List<Model.Entity> _Entities = null;
        private Model.Model _Model = null;
        public SetUpModel()
        {
            InitializeComponent();
            AddItemsCb();
        }
        private void AddItemsCb()
        {
            var models = Directory.GetDirectories(_Param.SAVE_PATH);
            for(int i = 0; i < models.Count(); i++)
            {
                string model = System.IO.Path.GetFileName(models[i]);
                cbModelsName.Items.Add(model); 
            }
        }
        private void diagram_PClick(object sender, MouseButtonEventArgs e)
        {
            System.Threading.Thread.Sleep(100);

            Grid g = sender as Grid;
            if (g != null)
            {

                int col = Grid.GetColumn(g);
                int row = Grid.GetRow(g);
                int cols = diagram.Cols;
                int index = row * cols + col;
                if (_Model != null)
                {
                    _Entity = _Model.Entities[index];
                    txt_entityName.Text = _Entity.Name;
                    txt_start.Text = _Entity.Start.ToString();
                    txt_end.Text = _Entity.End.ToString();
                    txt_Stt.Text = (index + 1).ToString();
                    bt_delete.Visibility = Visibility.Visible;
                    bt_update.Visibility = Visibility.Visible;
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void btSave_Inspection_Click(object sender, RoutedEventArgs e)
        {

        }

        private void bt_add_Click(object sender, RoutedEventArgs e)
        {
           
            try
            {
                _Stt = Convert.ToInt32(txt_Stt.Text);
                _Entity.Name = txt_entityName.Text.ToUpper();
                _Entity.Start = Convert.ToInt32(txt_start.Text);
                _Entity.End = Convert.ToInt32(txt_end.Text);
                _Entity.LenSn = txt_sn.Text.Length;
            }
            catch { };
            if (_Stt > _Entities.Count)
            {
                _Entities.Add(_Entity.Copy());
                _Stt += 1;
            }
            else
            {
                int index = Convert.ToInt32(txt_Stt.Text) - 1;
                _Entities[index] = _Entity.Copy();
                _Stt = _Entities.Count + 1;
                bt_add.Content = "ADD";
            }
            _Model.Entities = _Entities;
            int res = _Model.Save();
            diagram.Render(1, _Entities);
            clear();
            txt_Stt.Text = Convert.ToString(_Stt);
            

        }
        private void clear()
        {
            txt_entityName.Text = null;
            txt_start.Text = null;
            txt_end.Text = null;
            txt_Stt.Text = (_Model.Entities.Count()+1).ToString();
            txt_value.Text = null;
        }

        private void cbModelsName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _Model = Model.Model.LoadModel(cbModelsName.SelectedItem.ToString());
            if (_Model != null)
            {
                _Entities = _Model.Entities;
                _Stt = _Entities.Count + 1;
                txt_Stt.Text = _Stt.ToString();
                setUpEntity.IsEnabled = true;
                diagram.Render(1, _Entities);
            }
            else
                MessageBox.Show("Cant load model ", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void bt_addModel_Click(object sender, RoutedEventArgs e)
        {
            NewModel newModel = new NewModel(this);
            newModel.ShowDialog();
        }

        private void txt_sn_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int lengthSn = tb.Text.Length;
            txtStarSnLength.Text = lengthSn.ToString();
        }

        private void bt_update_Click(object sender, RoutedEventArgs e)
        {
            int index = Convert.ToInt32(txt_Stt.Text) - 1;
            _Model.Entities[index].Name = txt_entityName.Text;
            _Model.Entities[index].Start = Convert.ToInt32(txt_start.Text);
            _Model.Entities[index].End = Convert.ToInt32(txt_end.Text);
            _Model.Save();
            diagram.Render(1, _Entities);
            clear();
        }

        private void bt_delete_Click(object sender, RoutedEventArgs e)
        {
            int index = Convert.ToInt32(txt_Stt.Text) - 1;
            _Model.Entities.RemoveAt(index);
            _Model.Save();
            diagram.Render(1, _Model.Entities);
            clear();
        }

        private void txt_start_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            try
            {
                string sn = txt_sn.Text;
                int start = Convert.ToInt32(tb.Text);
                int end = Convert.ToInt32(txt_end.Text);
                txt_value.Text = sn.Substring(start, end - start);
                //int index = Convert.ToInt32(txt_Stt.Text) - 1;
                //if(_Entities.Count()> index)
                //    _Entities[index].Start = start;
            }
            catch { };
        }

        private void txt_end_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            try
            {
                string sn = txt_sn.Text;
                int start = Convert.ToInt32(txt_start.Text);
                int end = Convert.ToInt32(tb.Text);
                txt_value.Text = sn.Substring(start, end - start);
                //int index = Convert.ToInt32(txt_Stt.Text) - 1;
                //if (_Entities.Count() > index)
                //    _Entities[index].End = end;
            }
            catch { };
        }

        private void txt_entityName_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int index = Convert.ToInt32(txt_Stt.Text) - 1;
            //if (_Entities.Count() > index)
            //    _Entities[index].Name = tb.Text.ToUpper();
        }
    }
}
