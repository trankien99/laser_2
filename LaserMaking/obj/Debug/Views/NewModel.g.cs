﻿#pragma checksum "..\..\..\Views\NewModel.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "BB3C754F19BFF7A2FE034B2A08F61CC580016E71CC034EBC4974C76DD5997B6F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LaserMaking.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LaserMaking.Views {
    
    
    /// <summary>
    /// NewModel
    /// </summary>
    public partial class NewModel : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\Views\NewModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_modelName;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Views\NewModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_fileDesign;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Views\NewModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_openFileDesign;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Views\NewModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_save;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LaserMaking;component/views/newmodel.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\NewModel.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txt_modelName = ((System.Windows.Controls.TextBox)(target));
            
            #line 16 "..\..\..\Views\NewModel.xaml"
            this.txt_modelName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_modelName_TextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.txt_fileDesign = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\..\Views\NewModel.xaml"
            this.txt_fileDesign.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_fileDesign_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.bt_openFileDesign = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\Views\NewModel.xaml"
            this.bt_openFileDesign.Click += new System.Windows.RoutedEventHandler(this.bt_openFileDesign_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.bt_save = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\Views\NewModel.xaml"
            this.bt_save.Click += new System.Windows.RoutedEventHandler(this.bt_save_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

