﻿#pragma checksum "..\..\..\Views\SetUpModel.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "AD3A6C766BECF4FF8B573232228290579810D968EE47B8F3E66FCAE52264358D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LaserMaking.MyControl;
using LaserMaking.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LaserMaking.Views {
    
    
    /// <summary>
    /// SetUpModel
    /// </summary>
    public partial class SetUpModel : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 32 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel MenuTools;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbModelsName;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_addModel;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_sn;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtStarSnLength;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid setUpEntity;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel tableData;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_Stt;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_entityName;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_start;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_end;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_value;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_add;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_update;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bt_delete;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel dpDiagram;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal LaserMaking.MyControl.Diagram diagram;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\Views\SetUpModel.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btSave_Inspection;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LaserMaking;component/views/setupmodel.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\SetUpModel.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\..\Views\SetUpModel.xaml"
            ((LaserMaking.Views.SetUpModel)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.Window_Closing);
            
            #line default
            #line hidden
            return;
            case 2:
            this.MenuTools = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 3:
            this.cbModelsName = ((System.Windows.Controls.ComboBox)(target));
            
            #line 42 "..\..\..\Views\SetUpModel.xaml"
            this.cbModelsName.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbModelsName_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.bt_addModel = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\..\Views\SetUpModel.xaml"
            this.bt_addModel.Click += new System.Windows.RoutedEventHandler(this.bt_addModel_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txt_sn = ((System.Windows.Controls.TextBox)(target));
            
            #line 65 "..\..\..\Views\SetUpModel.xaml"
            this.txt_sn.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_sn_TextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.txtStarSnLength = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.setUpEntity = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.tableData = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 9:
            this.txt_Stt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.txt_entityName = ((System.Windows.Controls.TextBox)(target));
            
            #line 105 "..\..\..\Views\SetUpModel.xaml"
            this.txt_entityName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_entityName_TextChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.txt_start = ((System.Windows.Controls.TextBox)(target));
            
            #line 106 "..\..\..\Views\SetUpModel.xaml"
            this.txt_start.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_start_TextChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            this.txt_end = ((System.Windows.Controls.TextBox)(target));
            
            #line 107 "..\..\..\Views\SetUpModel.xaml"
            this.txt_end.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_end_TextChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.txt_value = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.bt_add = ((System.Windows.Controls.Button)(target));
            
            #line 109 "..\..\..\Views\SetUpModel.xaml"
            this.bt_add.Click += new System.Windows.RoutedEventHandler(this.bt_add_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.bt_update = ((System.Windows.Controls.Button)(target));
            
            #line 110 "..\..\..\Views\SetUpModel.xaml"
            this.bt_update.Click += new System.Windows.RoutedEventHandler(this.bt_update_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.bt_delete = ((System.Windows.Controls.Button)(target));
            
            #line 111 "..\..\..\Views\SetUpModel.xaml"
            this.bt_delete.Click += new System.Windows.RoutedEventHandler(this.bt_delete_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.dpDiagram = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 18:
            this.diagram = ((LaserMaking.MyControl.Diagram)(target));
            return;
            case 19:
            this.btSave_Inspection = ((System.Windows.Controls.Button)(target));
            
            #line 134 "..\..\..\Views\SetUpModel.xaml"
            this.btSave_Inspection.Click += new System.Windows.RoutedEventHandler(this.btSave_Inspection_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

